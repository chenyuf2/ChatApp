import React from 'react';
import "./Footer.css";


class Footer extends React.Component {
    CopyRight = ()=> {
        return (<h2 variant="body2" color="textSecondary" align="center">
            {'Copyright'} 
            {'Coding Cafe'}
            {new Date().getFullYear()}
            {'.'}
        </h2>)
    }
    render() {
        return (
            <footer>
                <div className="container">
                    <div className="row footer-padding">
                        <div className="col-md-8 col-sm-6 col-xs-12 text-center footer-text">
                           Copyright &copy; 2020 All Rights Reserved by Stephen Fan
                        </div>

                        <div class="col-md-4 col-sm-6 col-xs-12 text-center">
                            <ul class="social-icons">
                                <li><a class="facebook mr-2" href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a class="twitter ml-2 mr-2" href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a class="linkedin ml-2" href="#"><i class="fa fa-linkedin"></i></a></li>   
                            </ul>
                        </div>
                    </div>
                </div>
            </footer>
        )
    }
}

export default Footer;