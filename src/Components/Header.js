import React from 'react';
import "./Header.css";
import {Link} from 'react-router-dom';
function Header() {
    return (
        <nav className="navbar navbar-expand-lg">
        <a class="navbar-brand" href="/"><img src="https://image.flaticon.com/icons/png/128/2223/2223135.png"></img></a>
        
      
            <ul class="menu">
                <li className="menu-item"><Link to="/login" className="head-text">LogIn</Link></li>
                <li className="menu-item ml-5"><Link to="/signup" className="head-text">SignUp</Link></li>
            </ul>
      </nav>
    )
}

export default Header;


{/* <header className="header-login-signup">
<div className="header-limiter">
    <h1><a href="/">Coding<span>Cafe</span></a></h1>
    <nav>
        <Link to='/'>Home</Link>
        <a className="selected"><Link to="/">About App</Link></a>
        <a><Link to="/">Contact Us</Link></a>
    </nav>
    <ul>
        <li><Link to="/login">Login</Link></li>
        <li><Link to="/signup">Sign Up</Link></li>
    </ul>
</div>
</header> */}
