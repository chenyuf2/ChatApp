import React from 'react';
import * as firebase from 'firebase';

var firebaseConfig = {
    apiKey: "AIzaSyB--Rn8dKKAtl5C11V-BM-IpE-rCsgmzm8",
    authDomain: "messenger-f7fa3.firebaseapp.com",
    databaseURL: "https://messenger-f7fa3.firebaseio.com",
    projectId: "messenger-f7fa3",
    storageBucket: "messenger-f7fa3.appspot.com",
    messagingSenderId: "510288795072",
    appId: "1:510288795072:web:8d39a9a05c911a610ce425"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);

  export default firebase;