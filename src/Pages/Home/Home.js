import React, {Component} from 'react';
import Header from '../../Components/Header';
import Footer from '../../Components/Footer';
import './Home.css';
import Images from '../../ProjectImages/ProjectImages';
import {Link} from 'react-router-dom';
export default class Home extends Component {
    render() {
        return (
            <div>
            <Header></Header>
            <div className="container front-page">
                <div className="row center-img">
                    <div className="col-lg-6 col-sm-6 col-lg-6" style={{paddingLeft: "0", paddingRight: "0"}}>
                        <div className="front-title">
                            <h1 className="main-text">The Best Way</h1>
                            <h1 className="main-text">To Chat!</h1>
                            <h1 className="sub-text-title mt-4">A simple, powerful and elegant chat website for everyone</h1>
                            <Link to="/login" className="login-btn-front btn btn-lg">Login</Link>
                        </div>
                       
                    </div>
                    <div className="col-lg-6 col-sm-6 col-lg-6">
                        <img className="home-img" src={Images.apple}></img>
                    </div>
                </div>
            </div>
            <Footer></Footer>
            </div>
        )
    }
}

 {/* <div className="splash-container">
                <div className="splash">
                    <h1 className="splash-head">WEB CHAT APP</h1>
                    <p className="splash-subhead">
                        Let's talk about our loved ones
                    </p>
                    <div id="custom-button-wrapper">
                        <Link to="/login">
                            <a className="my-super-cool-btn">
                                <div className="dots-container">
                                    <div className="dot"></div>
                                    <div className="dot"></div>
                                    <div className="dot"></div>
                                    <div className="dot"></div>
                                    
                                </div>
                                <span className="buttoncooltext">Get Started</span>
                            </a>
                        </Link>
                    </div>
                </div>
            </div> */}