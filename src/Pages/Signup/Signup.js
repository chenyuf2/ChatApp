import React, {Component} from 'react';
import {Link}  from 'react-router-dom';
import './Signup.css';
import firebase from '../../Services/firebase';
// import {Card} from 'react-bootstrap';
// import CSSBaseLine from '@material-ui/core/CssBaseline';
// import TextField from '@material-ui/core/TextField';
// import Box from '@material-ui/core/Box';
// import Typography from '@material-ui/core/Typography';
import LoginString from "../Login/LoginStrings";
export default class Signup extends Component {
    constructor() {
        super();
        this.state = {
            email:"",
            password: "",
            name:"",
            error: null
        }
        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit =  this.handleSubmit.bind(this)
    }
    handleChange(event) {
        this.setState({
            [event.target.name]: event.target.value,
        });
    }
    async handleSubmit(event) {
        const {name, password, email} = this.state;
        event.preventDefault();
        document.getElementById("3").style.animation = "";
        try {
            firebase.auth().createUserWithEmailAndPassword(email, password)
            .then(async result=> {
                firebase.firestore().collection('users')
                .add({
                    name, 
                    id: result.user.uid,
                    email,
                    password,
                    URL:'',
                    messages:[{notificationId: "", number: 0}],
                    description:""
                }).then((docRef) => {
                    localStorage.setItem(LoginString.ID, result.user.uid);
                    localStorage.setItem(LoginString.Name, name);
                    localStorage.setItem(LoginString.Email, email);
                    localStorage.setItem(LoginString.Password, password);
                    localStorage.setItem(LoginString.PhotoUrl, "");
                    localStorage.setItem(LoginString.UPLOAD_CHANGED, "state_changed");
                    localStorage.setItem(LoginString.Description, "");
                    localStorage.setItem(LoginString.FirebaseDocumentId, docRef.id);
                    this.setState({
                        name: "",
                        password: "",
                        url:"",
                    })
                    this.props.history.push("/chat");
                })
                .catch((error) => {
                    console.log("error adding document");
                })
            })
        } catch(error) {
            console.log("2");
            document.getElementById("3").style.animation = "error-message 3.3s ease";
        }
    }
    render() {
        const paper = {
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
            height: "100vh"
        }
        return (
            <div>
                <Link className="corner-icon" to="/"><img className="icon-style" src="https://image.flaticon.com/icons/png/128/2223/2223135.png"></img></Link>
                <div style={paper}>
                    <form className="signup-form" noValidate onSubmit={this.handleSubmit}>
                        <input variant="outlined" placeholder="Email" className="common-form" margin="normal" required  id="email" fullWidth name="email" autoComplete="email" autoFocus onChange={this.handleChange} value={this.state.email}></input>
                        <input variant="outlined"  placeholder="Password" className="common-form mt-3"  margin="normal" required  id="password"  fullWidth  name="password" type="password" autoComplete="current-password" autoFocus onChange={this.handleChange} value={this.state.password}></input>
                        <input variant="outlined"  placeholder="Name" className="common-form mt-3 mb-3" margin="normal" required  id="name"  fullWidth name="name"  autoComplete="name" autoFocus onChange={this.handleChange} value={this.state.name}></input>
                        <div className="helper-container">
                            <button className="btn btn-lg signup-button" type="submit">
                                <span>Sign Up</span>
                            </button>
                        </div>
                        <div className="separator">Or</div>
                        <div className="helper-container">
                            <Link to="/login" className="login-button">
                                Login
                            </Link>
                        </div>
                        <div className="error-group" id="3">
                            <p id="1" style={{color: 'red'}}>Try again</p>
                        </div>

                    </form>
                </div>
               
            </div>
        )
    }
}