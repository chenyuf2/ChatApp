import React from 'react';
import "./Profile.css";
import ReactLoading from 'react-loading';
import 'react-toastify/dist/ReactToastify.css';
import firebase from "../../Services/firebase";
import images from "../../ProjectImages/ProjectImages";
import LoginString from "../Login/LoginStrings";
export default class Profile extends React.Component {
    constructor(props) {
        super(props);
        this.state={
            isLoading: false,
            documentKey: localStorage.getItem(LoginString.FirebaseDocumentId),
            id: localStorage.getItem(LoginString.ID),
            name: localStorage.getItem(LoginString.Name),
            aboutMe: localStorage.getItem(LoginString.Description),
            photoUrl: localStorage.getItem(LoginString.PhotoUrl),
        }
        this.newPhoto = null;
        this.newPhotoUrl = "";
    }
    componentDidMount() {
        if (!localStorage.getItem(LoginString.ID)) {
            this.props.history.push("/");
        }
    }
    onChangeNickname = (event)=> {
        this.setState({
            name: event.target.value
        })
    }
    onChangeAboutMe = (event) => {
        this.setState({
            aboutMe: event.target.value
        })
    }
    onChangeAvatar = (event) => {
        if (event.target.files && event.target.files[0]) {
            const prefixFiletype = event.target.files[0].type.toString();
            if (prefixFiletype.indexOf(LoginString.PREFIX_IMAGE) !== 0){
                this.props.showToast(0, "This file is not an image");
                return;
            }
            this.newPhoto = event.target.files[0];
            
            this.setState({
                photoUrl: URL.createObjectURL(event.target.files[0])
            })
        } else {
            this.props.showToast(0, "Something wrong with input file");
        }
    }
    uploadAvatar = ()=> {
        this.setState({
            isLoading: true
        })
       
        if (this.newPhoto) {
            const uploadTask = firebase.storage()
            .ref()
            .child(this.state.id)
            .put(this.newPhoto)
            uploadTask.on(
                LoginString.UPLOAD_CHANGED,
                null,
                err => {
                    this.props.showToast(0, err.message)
                },
                ()=> {
                    uploadTask.snapshot.ref.getDownloadURL().then(downloadURL => {
                        this.updateUserInfo(true, downloadURL)
                    })
                }
            )

        } else {
            this.updateUserInfo(false, null);
        }
    }
    updateUserInfo = (isUpdatedPhotoURL, downloadURL)=> {
        let newinfo;
        if (isUpdatedPhotoURL) {
            newinfo = {
                name: this.state.name,
                description: this.state.aboutMe,
                URL: downloadURL
            }
        } else {
            newinfo = {
                name: this.state.name,
                description: this.state.aboutMe,
            }
        }
        firebase.firestore().collection("users")
            .doc(this.state.documentKey)
            .update(newinfo)
            .then(data => {
                localStorage.setItem(LoginString.Name, this.state.name);
                localStorage.setItem(LoginString.Description, this.state.aboutMe);
                // console.log(isUpdatedPhotoURL);
                // console.log(downloadURL);
                if (isUpdatedPhotoURL) {
                    localStorage.setItem(LoginString.PhotoUrl, downloadURL)
                }
                this.setState({isLoading: false});
                this.props.showToast(1, "Update info success");
            })
    }
    render() {
        let tempImage = this.state.photoUrl;
        if (tempImage === undefined || tempImage === null || tempImage.length === 0) {
            tempImage = images.undraw;
        }
        return (    
            <div className="profileroot">
                <div className="profile-form">
                    <div className="image-center">
                    <img className="avatar" alt="" src={tempImage}></img>
                    </div>
               
                <div className="image-center">
                    <img className="imgInputFile"
                    alt="icon gallery"
                    src={images.choosefile}
                    onClick={()=>{this.refInput.click()}}>
                    </img>
                    <input ref={el => {
                        this.refInput = el
                    }} 
                    accept = "image/*"
                    className="viewInputFile"
                    type="file"
                    onChange={this.onChangeAvatar}></input>
                </div>
                <input className="profile-common-form mb-4" vlaue={this.state.name ? this.state.name: ""} placeholder="Your nickname..." onChange={this.onChangeNickname}></input>
                <input className="profile-common-form" value = {this.state.aboutMe ? this.state.aboutMe : ""} placeholder="Tell about yourself..." onChange={this.onChangeAboutMe}></input>
                <div className="container">
                    <div className="row mt-5 temp-center">
                        <div className="col-sm-6 col-md-6 col-lg-6">
                        <button className="btnUpdate" onClick={this.uploadAvatar}>save</button>
                        </div>
                        <div className="col-sm-6 col-md-6 col-lg-6">
                        <button className="btnback" onClick={()=>{this.props.history.push("/chat")}}>back</button>
                        </div>
                    </div>
                </div>
                {this.state.isLoading ? (
                    <div>
                        <ReactLoading
                        type={'spin'}
                        color={"#203152"}
                        height={"3%"}
                        width={"3%"}
                        />
                    </div>
                ):null}
                </div>
            </div>

        )
    }
}