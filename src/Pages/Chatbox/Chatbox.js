import React, { Children } from 'react';
import {Card} from 'react-bootstrap';
import ReactLoading from 'react-loading';
import 'react-toastify/dist/ReactToastify.css';
import firebase from '../../Services/firebase';
import Images from '../../ProjectImages/ProjectImages';
import moment from 'moment';
import './Chatbox.css';
import LoginString from '../Login/LoginStrings';
import 'bootstrap/dist/css/bootstrap.min.css';
import {toast, ToastContainer} from 'react-toastify';
import InputEmoji from 'react-input-emoji';
export default class ChatBox extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            isShowStiker: false,
            inputValue:"",
        }
        this.currentUserName = localStorage.getItem(LoginString.Name);
        this.currentUserId = localStorage.getItem(LoginString.ID);
        this.currentUserPhoto = localStorage.getItem(LoginString.PhotoUrl);
        this.currentUserDocumentId = localStorage.getItem(LoginString.FirebaseDocumentId);
        this.stateChanged = localStorage.getItem(LoginString.UPLOAD_CHANGED);
        this.currentPeerUser = this.props.currentPeerUser;
        this.groupChatId = null;
        this.currentPeerUserMessages = [];
        this.removeListener = null;
        this.currentPhotoFile = null;
        this.listMessage = [];
        firebase.firestore().collection("users").doc(this.currentPeerUser.documentKey).get()
        .then((docRef) => {
            this.currentPeerUserMessages = docRef.data().messages
        })
    }
    showToast = (type, message) => {
        switch (type) {
          case 0: 
            toast.warning(message);
            break;
          case 1:
            toast.success(message);
            default:
              break;
        }
      }
    componentDidUpdate(){
        this.scrollToBottom()
    }
    componentWillReceiveProps(newProps) {
        if (newProps.currentPeerUser) {
            this.currentPeerUser = newProps.currentPeerUser;
            this.getListHistory()
        }
    }
    componentDidMount() {
        this.getListHistory();
    }
    componentWillUnmount() {
        if (this.removeListener) {
            this.removeListener()
        }
    }

    getListHistory = ()=> {
        if (this.removeListener) {
            this.removeListener()
        } 
        this.listMessage.length = 0;
        this.setState({isLoading: true})
        if (this.hashString(this.currentUserId) <= this.hashString(this.currentPeerUser.id)) {
            this.groupChatId = `${this.currentUserId}-${this.currentPeerUser.id}`
        
        } else {
            this.groupChatId = `${this.currentPeerUser.id}-${this.currentUserId}`
        }
        // console.log(this.groupChatId);
        //Get History and listen
        // console.log(firebase.firestore().collection("users"));
        this.removeListener = firebase.firestore()
        .collection("Messages")
        .doc(this.groupChatId)
        .collection(this.groupChatId)
        .onSnapshot(Snapshot => {
            Snapshot.docChanges().forEach(change => {
                if (change.type === LoginString.DOC) {
                    this.listMessage.push(change.doc.data())
                }
            })
            this.setState({isLoading: false})
        },
         err => {
            this.showToast(0, err.toString())
        }
        );
       
    }
    onSendMessage = (content, type) => {
        let notificationMessages = [];
        if (this.state.isShowStiker && type === 2) {
            this.setState({isShowStiker: false})
        }
        if (content.trim() === '') {
            return
        }
        console.log(moment());
        const timestamp = moment().valueOf().toString();

        const itemMessage = {
            idFrom: this.currentUserId,
            idTo: this.currentPeerUser.id,
            timestamp: timestamp,
            content: content.trim(),
            type: type
        }
        firebase.firestore()
        .collection("Messages")
        .doc(this.groupChatId)
        .collection(this.groupChatId)
        .doc(timestamp)
        .set(itemMessage)
        .then(()=>{
            this.setState({inputValue: ''})
        })
        if ( document.getElementById(this.currentPeerUser.id + "last") !== null) {
            document.getElementById(this.currentPeerUser.id + "last").innerHTML = content;
        }
        if ( document.getElementById(this.currentPeerUser.id + "lastTime") !== null) {
            document.getElementById(this.currentPeerUser.id + "lastTime").innerHTML = moment(Number(timestamp)).format('MM/DD HH:mm');
        }
       
        this.currentPeerUserMessages.map((item) => {
            if (item.notificationId != this.currentUserId) {
                notificationMessages.push(
                    {
                        notificationId: item.notificationId,
                        number: item.number
                    }
                )
            }
        })
        firebase.firestore()
        .collection("users")
        .doc(this.currentPeerUser.documentKey)
        .update({
            messages: notificationMessages
        })
        .then((data)=>{})
        .catch(err => {
            this.showToast(0, err.toString())
        })

    }
    scrollToBottom = ()=> {
        if (this.messagesEnd) {
            this.messagesEnd.scrollIntoView({})
        }
    }
    onKeyboardPress = event => {
        // console.log(event);
        // if (event.key === "Enter") {
            this.onSendMessage(this.state.inputValue, 0);
        // }
    }
    openListSticker = () => {
        this.setState({isShowStiker: !this.state.isShowStiker})
    }
    onChoosePhoto = (event) => {
        if (event.target.files && event.target.files[0]) {
            this.setState({isLoading: true})
            this.currentPhotoFile = event.target.files[0];
            const prefixFiletype = event.target.files[0].type.toString();
            if (prefixFiletype.indexOf("image/") === 0) {
                this.uploadPhoto()
            } else{
                this.setState({isLoading: false});
                this.showToast(0, "This file is not an image");
            }
        } else {
            this.setState({isLoading: false});
        }
    }
    uploadPhoto = ()=> {
        if (this.currentPhotoFile) {
            const timestamp = moment()
            .valueOf()
            .toString()

            const uploadTask = firebase.storage()
            .ref()
            .child(timestamp)
            .put(this.currentPhotoFile)

            uploadTask.on(
                LoginString.UPLOAD_CHANGED,
                null,
                err=> {
                    this.setState({isLoading: false})
                   this.showToast(0, err.message)
                },
                ()=> {
                    uploadTask.snapshot.ref.getDownloadURL().then(downloadURL => {
                        this.setState({isLoading: false});
                        this.onSendMessage(downloadURL, 1);
                    })
                }
            )

        }
    }
    renderListMessage = ()=> {
        if (this.listMessage.length > 0) {
            let viewListMessage = [];
            this.listMessage.forEach((item, index) => {
                if (index === this.listMessage.length - 1) {
                    if (document.getElementById(this.currentPeerUser.id + "last") !== null) {
                        document.getElementById(this.currentPeerUser.id + "last").innerHTML = item.content;//important added
                    }
                    if ( document.getElementById(this.currentPeerUser.id + "lastTime") !== null) {
                        document.getElementById(this.currentPeerUser.id + "lastTime").innerHTML = moment(Number(item.timestamp)).format('MM/DD HH:mm');
                    }
                }
                if (item.idFrom === this.currentUserId) {
                    let tempImage = this.currentUserPhoto;
                    if (tempImage === undefined || tempImage === null || tempImage.length === 0) {
                        tempImage = Images.undraw;
                    }
                    if (item.type === 0) {
                        viewListMessage.push(
                            <div className="viewWrapItemRight" key={item.timestamp}>
                                <div className="viewWrapItemRight3">
                                
                            <div className="viewItemRight" key={item.timestamp}>
                                <span className="textContentItem">{
                                    item.content
                                }</span>
                            </div>
                                {this.isLastMessageRight(index) ? (
                                        <img
                                        src={tempImage}
                                        className="peerAvatarRight"
                                        ></img>
                                    ): (
                                        <div className="viewPaddingRight"></div>
                                    )} 
                            </div>
                             </div>
                        )
                    } else if (item.type === 1) {
                        viewListMessage.push(
                            <div className="viewWrapItemRight2" key={item.timestamp}>
                                <div className="viewWrapItemRight3">
                                    <div className="viewItemRight2" key={item.timestamp}>
                                        <img
                                        className="imgItemRight"
                                        src={item.content}
                                        alt="Please update your image"
                                        ></img>
                                    </div>
                                    {this.isLastMessageRight(index) ? (
                                        <img
                                        src={tempImage}
                                        className="peerAvatarRight"
                                        ></img>
                                    ): (
                                        <div className="viewPaddingRight"></div>
                                    )} 
                                </div>
                            </div>
                        )
                    } else {
                        viewListMessage.push(
                            <div className="viewWrapItemRight2" key={item.timestamp}>
                                <div className="viewWrapItemRight3">
                                    <div className="viewItemRight3" key={item.timestamp}>
                                        <img
                                        className="imgItemRight"
                                        src={this.getGifImage(item.content)}
                                        alt="content message"
                                        ></img>
                                    </div>
                                    {this.isLastMessageRight(index) ? (
                                        <img
                                        src={tempImage}
                                        className="peerAvatarRight"
                                        ></img>
                                    ): (
                                        <div className="viewPaddingRight"></div>
                                    )} 
                                </div>
                            </div>
                        )
                    }
                } else {
                    let tempImage = this.currentPeerUser.URL;
                    if (tempImage === undefined || tempImage === null || tempImage.length === 0) {
                        tempImage = Images.undraw;
                    }
                    if (item.type === 0) {
                        viewListMessage.push(
                            <div className="viewWrapItemLeft" key={item.timestamp}>
                                <div className="viewWrapItemLeft3">
                                    {this.isLastMessageLeft(index) ? (
                                        <img
                                        src={tempImage}
                                        className="peerAvatarLeft"
                                        ></img>
                                    ): (
                                        <div className="viewPaddingLeft"></div>
                                    )}
                                    <div className="viewItemLeft">
                                    <span className="textContentItem">{item.content}</span>
                                    </div>
                                </div>
                                {this.isLastMessageLeft(index) ? (
                                    
                                        <div className="time">
                                            {moment(Number(item.timestamp)).format('YYYY/MM/DD HH:mm')}
                                        </div>
                               
                                ): null}
                            </div>
                        )
                    } else if (item.type === 1) {
                        viewListMessage.push(
                            <div className="viewWrapItemLeft2" key={item.timestamp}>
                                <div className="viewWrapItemLeft3">
                                    {this.isLastMessageLeft(index) ? (
                                        <img
                                        src={tempImage}
                                        className="peerAvatarLeft"
                                        ></img>
                                    ): (
                                        <div className="viewPaddingLeft"></div>
                                    )}
                                    <div className="viewItemLeft2">
                                        <img
                                        src={item.content}
                                        alt="Content message"
                                        className="imgItemLeft"
                                        ></img>
                                    </div>
                                </div>
                                {this.isLastMessageLeft(index) ? (
                                   
                                        <div className="time">
                                            {moment(Number(item.timestamp)).format('YYYY/MM/DD HH:mm')}
                                        </div>
                                  
                                ): null}
                            </div>
                        )
                    } else {
                        viewListMessage.push(
                            <div className="viewWrapItemLeft2" key={item.timestamp}>
                                <div className="viewWrapItemLeft3">
                                    {this.isLastMessageLeft(index) ? (
                                        <img
                                        src={tempImage}
                                        className="peerAvatarLeft"
                                        ></img>
                                    ): (
                                        <div className="viewPaddingLeft"></div>
                                    )}
                               
                                <div className="viewItemLeft3" key={item.timestamp}>
                                    <img
                                    className="imgItemRight"
                                    src={this.getGifImage(item.content)}
                                    alt="content message"
                                    ></img>
                                </div>
                            </div>
                                {this.isLastMessageLeft(index) ? (
                                    
                                        <div className="time">
                                            {moment(Number(item.timestamp)).format('YYYY/MM/DD HH:mm')}
                                        </div>
                                  
                                ): null}
                            </div>
                        )
                    }
                }
            }) 
            return viewListMessage;
        } else {
            return (
                <div className="viewWrapSayHi">
                    <span className="textSayHi">Say Hi to new Friend</span>
                    {/* <img className="imgWaveHand"
                    src={Images.sticker}
                    alt="wave hand"
                    >
                    </img> */}
                </div>
            )
        }
    }
    addEmoji = e => {
        let emoji = e.native;
        let temmp = this.state.inputValue + emoji;
        this.setState({
          inputValue: temmp
        });
      };
      logEmoji =(emoji) => {
          console.log(emoji);
        let temp = this.state.inputValue + emoji.unicode;
        this.setState({
            inputValue: temp
        })
      }
    render() {
        let tempImage = this.currentPeerUser.URL;
        if (tempImage === undefined || tempImage === null || tempImage.length === 0) {
            tempImage = Images.undraw;
        }
        return (
            <div className="viewChatBoard">
                <div className="headerChatBoard">
                    <img 
                    className="viewAvatarItem ml-3"
                    src={tempImage}
                    alt=""
                    ></img>
                    <div className="textHeaderChatBoard">
                        <p className="name-title">{this.currentPeerUser.name}</p>
                    </div>
                    {/* <div className="aboutme">
                        <span>
                            <p>{this.currentPeerUser.description}</p>
                        </span>
                    </div> */}
                </div>
                <div className="viewListContentChat">
                    {this.renderListMessage()}
                    <div style={{float: 'left', clear: 'both'}} ref={el=>{
                        this.messagesEnd = el
                    }}>

                    </div>
                </div>
                {this.state.isShowStiker ? this.renderSticker() : null}
                <div className="viewBottom">
               
                    {/* <button className="select-smile-btn" onClick={this.openListSticker}>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" className="select-smile">
                        <circle cx="12" cy="12" r="10" />
                        <path d="M8 14s1.5 2 4 2 4-2 4-2M9 9h.01M15 9h.01" /></svg>
                    </button> */}
                    {/* <input className="viewInput"
                    placeholder="Type a message"
                    value = {this.state.inputValue}
                    onChange={event => {
                        this.setState({inputValue: event.target.value})
                    }}
                    onKeyPress = {this.onKeyboardPress}
                    ></input> */}
                     <InputEmoji
                     className="viewInput-temp"
                     placeholder="Type a message"
                     value = {this.state.inputValue}
                     onChange={event => {
                        this.setState({inputValue: event})
                    }}
                    cleanOnEnter
                    onEnter = {this.onKeyboardPress}
                    style={{width: '100px'}}
                    />
                     <button className="select-pic-btn" onClick={()=>{this.refInput.click()}}>
                    <svg className="select-pic" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" >
                    <rect x="3" y="3" width="18" height="18" rx="2" ry="2" />
                    <circle cx="8.5" cy="8.5" r="1.5" />
                    <path d="M21 15l-5-5L5 21" /></svg>
                    </button>
                    <input
                    className="viewInputGallery"
                    accept = "images/*"
                    type="file"
                    ref={el=>{this.refInput = el}}
                    onChange={this.onChoosePhoto}
                    ></input>
                    <img
                    className="icSend"
                    src={Images.send}
                    alt="icon send"
                    onClick={()=>{
                        this.onSendMessage(this.state.inputValue, 0)
                    }}
                    ></img>

                </div>
                {this.state.isLoading ? (
                    <div className="viewLoading">
                        <ReactLoading type={"spin"}
                        color={"#203152"}
                        height={"3%"}
                        width={"3%"}
                        ></ReactLoading>
                    </div>
                ):null}
            </div>
        )
    }
    renderSticker= ()=> {
        return (
            <div className="viewStickers">
                <img className="imgSticker"
                src={Images.sticker}
                alt="sticker"
                onClick={()=>{this.onSendMessage("sticker", 2)}}
                >
                </img>
            </div>
        )
    }
    hashString = str => {
        let hash = 0;
        for (let i = 0; i < str.length; i++) {
            hash += Math.pow(str.charCodeAt(i) * 31, str.length - i)
            hash  = hash & hash
        }
        return hash;
    }
    isLastMessageLeft(index) {
        if (
            (index + 1 < this.listMessage.length && 
                this.listMessage[index + 1].idFrom === this.currentUserId) || 
                index === this.listMessage.length - 1 
        ) {
            return true;
        } else {
            return false;
        }
    }
    isLastMessageRight(index) {
        if (
            (index + 1 < this.listMessage.length &&
                this.listMessage[index + 1].idFrom !== this.currentUserId) ||
                index === this.listMessage.length - 1
        ) {
            return true;
        } else {
            return false;
        }
    }
    getGifImage = value => {
        switch (value) {
            case "sticker":
                return Images.sticker
            default:
                return Images.camera
        }
    }
}