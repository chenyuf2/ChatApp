import React from 'react';
import LoginString from "../Login/LoginStrings";
import firebase from "../../Services/firebase";
import "./Chat.css";
import ReactLoading from 'react-loading';
import ChatBox from "../Chatbox/Chatbox";
import WelcomeCard from "../Welcome/Welcome";
import Images from '../../ProjectImages/ProjectImages';
import ifIsImage from 'if-is-image';
import moment from 'moment';
export default class Chat extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            isOpenDialogConfirmLogout: false,
            currentPeerUser: null,
            displayedContactSwitchedNotification: [],
            displayedContacts: [],
            lastMessages:[],
            
           
        }
        this.currentUserName = localStorage.getItem(LoginString.Name);
        this.currentUserId = localStorage.getItem(LoginString.ID);
        this.currentUserPhoto = localStorage.getItem(LoginString.PhotoUrl);
        this.currentUserDocumentId = localStorage.getItem(LoginString.FirebaseDocumentId);
        this.currentUserMessages = [];
        this.searchUsers = [];
        this.notificationMessagesErase = [];
        this.onProfileClick = this.onProfileClick.bind(this);
        this.getListUser= this.getListUser.bind(this);
        this.renderListUser = this.renderListUser.bind(this);
        this.getClassnameforUserandNotification = this.getClassnameforUserandNotification.bind(this);
        this.notificationErase = this.notificationErase.bind(this);
        this.updaterenderList = this.updaterenderList.bind(this);
        this.getLastMessage = this.getLastMessage.bind(this);
        this.currentLastMessage = {};
        this.LastMessageTime = {};
    }

    
  
    logout=()=> {
        firebase.auth().signOut();
        this.props.history.push("/");
        localStorage.clear();
    }
    onProfileClick = ()=> {
        this.props.history.push("/profile");
    }
    componentDidMount = ()=> {
        firebase.firestore().collection('users').doc(this.currentUserDocumentId).get()
        .then((doc) => {
            doc.data().messages.map((item) => {
                this.currentUserMessages.push({
                    notificationId: item.notificationId,
                    number: item.number
                })
            })
            this.setState({
                displayedContactSwitchedNotification: this.currentUserMessages
            })
        })
        this.getListUser();
    }
  
    getListUser = async()=> {
        const result = await firebase.firestore().collection('users').get();
       

        if (result.docs.length > 0) {
            let listUsers = [];
            listUsers = [...result.docs];
            listUsers.forEach((item, index)=> {
                this.searchUsers.push({
                    key: index,
                    documentKey: item.id,
                    id: item.data().id,
                    name: item.data().name,
                    messages: item.data().messages,
                    URL: item.data().URL,
                    description: item.data().description
                }
            )
            })
            for (let i = 0; i < this.searchUsers.length; i++) {
                let tempUser = this.searchUsers[i];
                let groupChatId;
                if (this.hashString(this.currentUserId) <= this.hashString(tempUser.id)) {
                    groupChatId = `${this.currentUserId}-${tempUser.id}`
                
                } else {
                    groupChatId = `${tempUser.id}-${this.currentUserId}`
                }
                const tempResult = await firebase.firestore().collection("Messages").doc(groupChatId).collection(groupChatId).get();
                if (tempResult.docs.length > 0) {
                    // console.log(tempResult.docs[tempResult.docs.length - 1].data());
                    this.LastMessageTime[tempUser.id] = moment(Number(tempResult.docs[tempResult.docs.length - 1].data().timestamp)).format('MM/DD HH:mm');
                    this.currentLastMessage[tempUser.id] = tempResult.docs[tempResult.docs.length - 1].data().content;
                }
            }
            this.setState({
                isLoading: false
            })
        }
        this.renderListUser()
    }

    getClassnameforUserandNotification  = (itemId) => {
        let number = 0;
        let classname = "";
        let check = false;
        if (this.state.currentPeerUser && this.state.currentPeerUser.id === itemId) {
            classname = "viewWrapItemFocused";
        } else {
            this.state.displayedContactSwitchedNotification.forEach((item)=> {
                if (item.notificationId.length > 0) {
                    if (item.notificationId === itemId) {
                        check = true;
                        number = item.number; 
                    }
                }
            })
            if (check === true) {
                classname = "viewWrapItemNotification"
            } else {
                classname = "viewWrapItem-temp";
            }
        }
        return classname;
    }
    getLastMessage = () => {
        if (this.searchUsers.length > 0) {
            this.searchUsers.map((item) => {
                if (item.id != this.currentUserId) {
                    let currentChatId;
                    if (this.hashString(this.currentUserId) <= this.hashString(item.id)) {
                        currentChatId = `${this.currentUserId}-${item.id}`
                    
                    } else {
                        currentChatId = `${item.id}-${this.currentUserId}`
                    } 
                    firebase.firestore()
                    .collection("Messages")
                    .doc(currentChatId)
                    .collection(currentChatId)
                    .onSnapshot(Snapshot => {
                        if (Snapshot.docChanges().length > 0) {
                            let temp = this.state.lastMessages.concat([Snapshot.docChanges()[Snapshot.docChanges().length - 1].doc.data().content.slice(1)]);
                            this.setState({lastMessages: temp});
                        }
                    },
                    err => {
                        this.showToast(0, err.toString())
                    }
                    );
                }
            })
        }
        // console.log(this.currentLastMessage);
        
    }
    renderListUser = ()=> {
        if (this.searchUsers.length > 0) {
            let viewListUser = [];
            let classname = "";
            this.searchUsers.map((item) => {
                if (item.id != this.currentUserId) {
                    let currentChatId;
                    if (this.hashString(this.currentUserId) <= this.hashString(item.id)) {
                        currentChatId = `${this.currentUserId}-${item.id}`
                    
                    } else {
                        currentChatId = `${item.id}-${this.currentUserId}`
                    } 

                    classname = this.getClassnameforUserandNotification(item.id); 
                    let tempImage = item.URL;
                    if (tempImage === undefined || tempImage === null || tempImage.length === 0) {
                        tempImage = Images.undraw;
                    } 
                    let tempDescription = item.description;
                    if (tempDescription.length === 0) {
                        tempDescription = "No description"
                    }
                    
 
                    let lastMessage = this.currentLastMessage[item.id];
                    if (lastMessage === undefined || lastMessage === null || lastMessage.length === 0) {
                        lastMessage = "Say Hi to new Friend";
                    } 

                    let lastTime = this.LastMessageTime[item.id];
                    if (lastTime === undefined || lastTime === null || lastTime.length === 0) {
                        lastTime = "";
                    }
                   
                    viewListUser.push(
                            <button style={{display: "flex", width: "100%", padding: '1rem'}} id={item.id} className = {classname} onClick={()=>{
                                this.notificationErase(item.id); 
                                this.setState({currentPeerUser: item}); 
                                // document.getElementById(item.id).classList.toggle()
                                for (let i = 0; i < this.searchUsers.length; i++) {
                                    if (document.getElementById('' + this.searchUsers[i].id) !== null) {
                                        document.getElementById("" + this.searchUsers[i].id).classList.remove("active");
                                    }
                                }
                                    document.getElementById(item.id).classList.add("active");
                                }}>
                                <img className = "viewAvatarItem" src={tempImage} alt="" placeholder = ""></img>
                                <div className="viewWrapContentItem ml-3">
                                    <div className="textItem">
                                        <div style={{display: 'flex'}}>{`${item.name}`}</div>
                                        <div style={{display: 'flex'}} className="small-textItem" id={item.id + "lastTime"}>{lastTime}</div>
                                    </div>
                                    <span className="sub-textItem" id={item.id + "last"}>
                                        {lastMessage}
                                    </span>
                                    {/* <span className="sub-textItem">
                                        {this.state.currentLastMessage}
                                    </span> */}
                                </div>
                                {classname === "viewWrapItemNotification" ?
                                <div className="notificationpragraph">
                                    <p id={item.key} className = "newmessages">New messages</p>
                                </div>:null}
                            </button>
                    )
                }
            })
           
            this.setState({
                displayedContacts: viewListUser
            })
        } else {
            console.log("No user is present");
        }
    }
    notificationErase = (itemId)=> {
        this.state.displayedContactSwitchedNotification.forEach((el)=> {
            if (el.notificationId.length > 0) {
                if (el.notificationId != itemId) {
                    this.notificationMessagesErase.push({
                        notificationId: el.notificationId,
                        number: el.number
                    })
                }
            }
        })
        this.updaterenderList()
    }
    updaterenderList = ()=> {
        firebase.firestore().collection('users').doc(this.currentUserDocumentId).update(
            {messages: this.notificationMessagesErase}
        )
        this.setState({
            displayedContactSwitchedNotification: this.notificationMessagesErase
        })
    }
    hashString = str => {
        let hash = 0;
        for (let i = 0; i < str.length; i++) {
            hash += Math.pow(str.charCodeAt(i) * 31, str.length - i)
            hash  = hash & hash
        }
        return hash;
    }
    searchHandler = (event)=> {
      let searchQuery = event.target.value.toLowerCase(),
      displayedContacts = this.searchUsers.filter((el) => {
          let SearchValue = el.name.toLowerCase();
          return SearchValue.indexOf(searchQuery) !== -1;
      })
      this.displayedContacts = displayedContacts;
      this.displaySearchContacts();
    }
    displaySearchContacts = ()=> {
        if (this.searchUsers.length > 0) {
            let viewListUser = [];
            let classname = "";

            this.displayedContacts.map((item) => {
                if (item.id != this.currentUserId) {
                    classname = this.getClassnameforUserandNotification(item.id);
                    let tempImage = item.URL;
                    if (tempImage === undefined || tempImage === null || tempImage.length === 0) {
                        tempImage = Images.undraw;
                    } 
                    let tempDescription = item.description;
                    if (tempDescription.length === 0) {
                        tempDescription = "No description"
                    }
                    let lastMessage = this.currentLastMessage[item.id];
                    if (lastMessage === undefined || lastMessage === null || lastMessage.length === 0) {
                        lastMessage = "Say Hi to new Friend";
                    } 
                    viewListUser.push(
                        <button style={{display: "flex", width: "100%", padding: '1rem', transition: '0s'}} id={item.id} className = {classname} onClick={()=>{
                            this.notificationErase(item.id); 
                            this.setState({currentPeerUser: item}); 
                            // for (let i = 0; i < this.searchUsers.length; i++) {
                            //     if (document.getElementById('' + this.searchUsers[i].id) !== null) {
                            //         document.getElementById("" + this.searchUsers[i].id).style.borderLeft = "";
                            //     }
                            // }
                            //     document.getElementById(item.id).style.borderLeft = "4px solid #0086ff";
                          
                            }}>
                            <img className = "viewAvatarItem" src={tempImage} alt="" placeholder = ""></img>
                            <div className="viewWrapContentItem ml-3">
                                    <span className="textItem">
                                        {`${item.name}`}
                                    </span>
                                    <span className="sub-textItem" id={item.id + "last"}>
                                        {lastMessage}
                                    </span>
                                </div>
                                {classname === "viewWrapItemNotification" ?
                                <div className="notificationpragraph">
                                    <p id={item.key} className = "newmessages">New messages</p>
                                </div>:null}
                        </button>
                    )
                }
            })
            
            this.setState({
                displayedContacts: viewListUser
            })
        } else {
            console.log("No user is present");
        }
    }
    render() {
        let tempImage = this.currentUserPhoto;
        if (tempImage === undefined || tempImage === null || tempImage.length === 0) {
            tempImage = Images.undraw;
        }
        return (
            <div className="root" >
                   <div className="row" style={{margin: '0'}}>
                   <div className="col-md-3 col-sm-3 col-lg-3 col-xl-3 left-container">
                       <div className="profileviewleftside">
                          
                           <div className="" style={{justifyContent: 'center', alignContent: 'center', display: 'flex'}}>
                                <img className="ProfilePicture ml-3" alt="" src={tempImage} onClick={this.onProfileClick}></img>
                                {/* <p className="message-text">Messages</p> */}
                                <div className="message-text ml-3">
                                    Messages
                                </div>
                           </div>
                            <div className="ml-3" >
                            <a class="dark-light btn btn-sm" onClick={()=>{
                              document.body.classList.toggle("dark-mode");
                            }}>
                                <svg viewBox="0 0 24 24" stroke="currentColor" stroke-width="1.5" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                <path d="M21 12.79A9 9 0 1111.21 3 7 7 0 0021 12.79z" /></svg>
                            </a>
                                <a onClick = {this.logout} id="logout-btn" className="btn btn-sm" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <svg className="svg-style" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round">
                                    <circle cx="12" cy="12" r="3" />
                                    <path d="M19.4 15a1.65 1.65 0 00.33 1.82l.06.06a2 2 0 010 2.83 2 2 0 01-2.83 0l-.06-.06a1.65 1.65 0 00-1.82-.33 1.65 1.65 0 00-1 1.51V21a2 2 0 01-2 2 2 2 0 01-2-2v-.09A1.65 1.65 0 009 19.4a1.65 1.65 0 00-1.82.33l-.06.06a2 2 0 01-2.83 0 2 2 0 010-2.83l.06-.06a1.65 1.65 0 00.33-1.82 1.65 1.65 0 00-1.51-1H3a2 2 0 01-2-2 2 2 0 012-2h.09A1.65 1.65 0 004.6 9a1.65 1.65 0 00-.33-1.82l-.06-.06a2 2 0 010-2.83 2 2 0 012.83 0l.06.06a1.65 1.65 0 001.82.33H9a1.65 1.65 0 001-1.51V3a2 2 0 012-2 2 2 0 012 2v.09a1.65 1.65 0 001 1.51 1.65 1.65 0 001.82-.33l.06-.06a2 2 0 012.83 0 2 2 0 010 2.83l-.06.06a1.65 1.65 0 00-.33 1.82V9a1.65 1.65 0 001.51 1H21a2 2 0 012 2 2 2 0 01-2 2h-.09a1.65 1.65 0 00-1.51 1z" /></svg>
                                </a>
                               
                           </div>
                           {/* <button className="btn Logout" onClick = {this.logout}><i className="fa fa-square"></i></button> */}
                       </div>
                       {/* <div className="rootsearchbar">
                            <div className="input-container mt-3">
                                <i className="fa fa-search icon"></i>
                                <input className="input-field"
                                type="text"
                                onChange={this.searchHandler}
                                placeholder="Search">
                                </input>

                            </div>
                       </div> */}
                       {this.state.displayedContacts}
                   </div>
                       {this.state.currentPeerUser ? (
                            <div className="col-md-6 col-sm-6 col-lg-6 col-xl-6" style={{padding: '0'}}>
                           <ChatBox 
                           currentPeerUser = {this.state.currentPeerUser}
                           showToast = {this.showToast}
                           ></ChatBox> </div>) : ( <div className="col-md-9 col-sm-9 col-lg-9 col-xl-9" style={{padding: '0'}}><WelcomeCard currentUserName = {this.currentUserName}
                           currentUserPhoto = {this.currentUserPhoto}></WelcomeCard></div>
                       )}
                     {this.state.currentPeerUser ? (
                   <div className="col-md-3 col-sm-3 col-lg-3 col-xl-3 rest-space" style={{padding: '0'}}>
                            <div className="empty-block">
                                <div className="logo mr-3">
                            <svg viewBox="0 0 513 513" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
    <path d="M256.025.05C117.67-2.678 3.184 107.038.025 245.383a240.703 240.703 0 0085.333 182.613v73.387c0 5.891 4.776 10.667 10.667 10.667a10.67 10.67 0 005.653-1.621l59.456-37.141a264.142 264.142 0 0094.891 17.429c138.355 2.728 252.841-106.988 256-245.333C508.866 107.038 394.38-2.678 256.025.05z" />
    <path d="M330.518 131.099l-213.825 130.08c-7.387 4.494-5.74 15.711 2.656 17.97l72.009 19.374a9.88 9.88 0 007.703-1.094l32.882-20.003-10.113 37.136a9.88 9.88 0 001.083 7.704l38.561 63.826c4.488 7.427 15.726 5.936 18.003-2.425l65.764-241.49c2.337-8.582-7.092-15.72-14.723-11.078zM266.44 356.177l-24.415-40.411 15.544-57.074c2.336-8.581-7.093-15.719-14.723-11.078l-50.536 30.744-45.592-12.266L319.616 160.91 266.44 356.177z" fill="#fff" /></svg>
                                </div>
                            </div>
                            <div className="detail-block">
                                <img className="detail-pic" src={this.state.currentPeerUser.URL.length === 0 ? Images.undraw: this.state.currentPeerUser.URL}></img>
                                <div className="mt-3">
                                    <p className="detail-name">{this.state.currentPeerUser.name}</p>
                                </div>
                                <div className="">
                                    <p className="detail-description">{
                                    this.state.currentPeerUser.description.length === 0 ? 
                                    "No description" :  this.state.currentPeerUser.description
                                    }</p>
                                </div>
                                <div className="btn-group mt-2">
                                    <button className="detail-button mr-1">
                                    <svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg" fill="currentColor" stroke="currentColor" stroke-width="0" stroke-linecap="round" stroke-linejoin="round" >
                                    <path d="M22 16.92v3a2 2 0 01-2.18 2 19.79 19.79 0 01-8.63-3.07 19.5 19.5 0 01-6-6 19.79 19.79 0 01-3.07-8.67A2 2 0 014.11 2h3a2 2 0 012 1.72 12.84 12.84 0 00.7 2.81 2 2 0 01-.45 2.11L8.09 9.91a16 16 0 006 6l1.27-1.27a2 2 0 012.11-.45 12.84 12.84 0 002.81.7A2 2 0 0122 16.92z" />
                                    </svg>Call Group</button>
                                    <button className="detail-button ml-1"> <svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg" fill="currentColor" stroke="currentColor" stroke-width="0" stroke-linecap="round" stroke-linejoin="round" >
                                    <path d="M23 7l-7 5 7 5V7z" />
                                    <rect x="1" y="5" width="15" height="14" rx="2" ry="2" /></svg>Video Chat</button>
                                </div>
                            
                                <div className="mt-5" style={{width: "100%"}}>
                                    <input className="search-conversion"
                                    type="text"
                                    placeholder="Search in Conversion">
                                    </input>
                                    <div className="detail-change">
                                        Change Color
                                        <div className="colors">
                                            <div className="color selected-color" style={{backgroundColor: '#0086ff'}}></div>
                                            <div className="color" style={{backgroundColor: '#9f7aea'}}></div>
                                            <div className="color" style={{backgroundColor: '#38b2ac'}}></div>
                                            <div className="color" style={{backgroundColor: '#ed8936'}}></div>
                                        </div>
                                    </div>
                                    <div className="detail-change">
                                        Change Emoji
                                        <div className="colors">
                                            <i className="fa fa-thumbs-up color fa-lg" style={{height: '20px', width: '20px'}}></i>
                                        </div>
                                    </div>
                                </div>
                              
                            </div>

                   </div>) : null}
                   </div>
            </div>
        )
    }
}