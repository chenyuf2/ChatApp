import React from 'react';
import 'react-toastify/dist/ReactToastify.css';
import './Welcome.css';
import Images from '../../ProjectImages/ProjectImages';
export default class WelcomeCard extends React.Component {
    render() {
        let tempImage = this.props.currentUserPhoto;
        if (tempImage === undefined || tempImage === null || tempImage.length === 0) {
            tempImage = Images.undraw;
        }
        return (
            <div className="viewWelcomeBoard">
                <img className="avatarWelcome" src={Images.chatBack}
                alt=""></img>
                {/* <span className="textTileWelcome">{`Welcome, ${this.props.currentUserName}`}</span>
                <span className="textDescriptionWelcome">
                    Let's Chat!
                </span> */}
            </div>
        )
    }
}