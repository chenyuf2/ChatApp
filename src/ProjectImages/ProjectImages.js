const Images = {
    ali: require('../Images/macbook_pro.jpg'),
    choosefile: require('../Images/camera.png'),
    sticker: require('../Images/sticker.jpg'),
    send: require('../Images/send.png'),
    interface: require('../Images/interface.png'),
    commerce: require('../Images/commerce.png'),
    macbookPro: require('../Images/macbookpro.jpg'),
    apple: require('../Images/apple.jpg'),
    undraw: require('../Images/undraw.png'),
    chatBack: require('../Images/chatBackground.png'),
    emoji1: require('../Images/emojis/emoji1.png'),
    emoji2: require('../Images/emojis/emoji2.png'),
    emoji3: require('../Images/emojis/emoji3.png'),
    emoji4: require('../Images/emojis/emoji4.png'),
    emoji5: require('../Images/emojis/emoji5.png'),
    emoji6: require('../Images/emojis/emoji6.png'),
    emoji7: require('../Images/emojis/emoji7.png'),
    emoji8: require('../Images/emojis/emoji8.png'),
    emoji9: require('../Images/emojis/emoji9.png'),
    emoji10: require('../Images/emojis/emoji10.png'),




}
export default Images;